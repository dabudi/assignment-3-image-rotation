#include "manager.h"
#include "image.h"
#include "status.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_ARG 4

int main(int argc, char *argv[NUM_ARG]) {
    if (argc != NUM_ARG) {
        fprintf(stderr, "4 args expected");
        return EXIT_FAILURE;
    }

    FILE *input = NULL;
    FILE *output = NULL;

    if (!(open_file(&input, argv[1], "rb"))) {
        fprintf(stderr, "Error reading input file");
        return EXIT_FAILURE;
    }

    struct image source = {0};

    if (from_bmp(input, &source)!= READ_OK) {
        fprintf(stderr, "Error opening bmp file");
        return EXIT_FAILURE;
    }

    close_file(input);

    int angle = atoi(argv[3]);
    struct image result = {0};

    switch (angle) {
        case 0: 
            result = getImage(&source);;
            break;
        case 90:
        case -270:
            result = rotate_90(source);
            break;
        case 180:
        case -180:
            result = rotate_180(source);
            break;
        case 270:
        case -90:
            result = rotate_270(source);
            break;
        default:
            fprintf(stderr, "Angle must be: 90, -90, 180, -180, 270, -270");
            empty_image(&source);
            return EXIT_FAILURE;
    }

    if (!(open_file(&output, argv[2], "wb"))) {
        fprintf(stderr, "Error opening output file");
        empty_image(&source);
        empty_image(&result);
        return EXIT_FAILURE;
    }

    if (to_bmp(output, &result) != WRITE_OK) {
        fprintf(stderr, "Error saving bmp file");
        return EXIT_FAILURE;
    }

    empty_image(&source);
    empty_image(&result);
    close_file(output);
    

    return EXIT_SUCCESS;
}
