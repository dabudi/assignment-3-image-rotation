#include "status.h"
#include "image.h"
#include <stdlib.h>

#define BF_TYPE 0x4D42
#define HEADER_SZ sizeof(struct bmp_header)
#define PIXEL_SZ sizeof(struct pixel)
#define BMP_HEADER_SIZE 40
#define BITS_PER_PIXEL 24


#pragma pack(push, 1)

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)

static uint8_t padding(uint64_t width) {
    return 4 - width * PIXEL_SZ % 4;
}

struct bmp_header create_header(struct image const *img){
    return (struct bmp_header)
    {
        .bfType = BF_TYPE,
        .bfileSize = HEADER_SZ + img->width * img->height * PIXEL_SZ,
        .bfReserved = 0,
        .bOffBits = HEADER_SZ,
        .biSize = BMP_HEADER_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = BITS_PER_PIXEL,
        .biCompression = 0,
        .biSizeImage = img->width * img->height * PIXEL_SZ,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE *in, struct image *img) {

    struct bmp_header header;
    fread(&header, HEADER_SZ, 1, in);

    if (header.bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    img->height = header.biHeight;
    img->width = header.biWidth;

    img->data = (struct pixel *)malloc(img->width * img->height * PIXEL_SZ);

    fseek(in, header.bOffBits, SEEK_SET);

    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            fread(&(img->data[i * img->width + j]), PIXEL_SZ, 1, in);
        }

        fseek(in, (long)padding(img->width), SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    struct bmp_header header = create_header(img);

    fwrite(&header, HEADER_SZ, 1, out);

    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            fwrite(&(img->data[i * img->width + j]), PIXEL_SZ, 1, out);
        }

        uint8_t zero = 0;
        for (size_t k = 0; k < padding(img->width); ++k) {
            fwrite(&zero, sizeof(uint8_t), 1, out);
        }
    }

    return WRITE_OK;
}
