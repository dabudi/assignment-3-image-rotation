#include "../include/manager.h"
#include <stdio.h>

bool open_file(FILE **file, const char *name, const char *mode) {
    *file = fopen(name, mode);
    return *file != NULL;
}

bool close_file(FILE* file) {
    if (file) return !fclose(file);
    else return false;
}
