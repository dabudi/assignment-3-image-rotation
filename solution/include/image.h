#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#pragma pack(push, 1)

struct pixel {
    uint8_t b, g, r;
};

#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image make_image(const uint64_t width, const uint64_t height);

void empty_image(struct image *img);

struct image getImage(const struct image* original);

struct image rotate_90(const struct image source);

struct image rotate_180(const struct image source);

struct image rotate_270(const struct image source);

#endif
